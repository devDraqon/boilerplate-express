var express = require('express');
var router = express.Router();
var cors = require('cors');

router.options('/*', cors(), function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  res.sendStatus(200);
});

router.get('/get/', cors(), function(req, res, next) {
  res.send({message: "GET Request Works"});
});

router.post('/post/', cors(), function(req, res, next) {
  res.send({copySentData: req.body})
});

module.exports = router;
